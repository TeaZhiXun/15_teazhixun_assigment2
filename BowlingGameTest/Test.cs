﻿using NUnit.Framework;
using System;
using BowlingGameKata;
namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp]
        public void SetupGame()
        {
            game = new Game();
        }
        public void RollMany(int rolls,int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }
        [Test()]
        public void CutterGame()
        {

            RollMany(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }
        [Test()]
        public void RollOnes()
        {
            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }
        [Test()]
        public void Rollsparefirstframe()
        {
            game.Roll(9);game.Roll(1);
            RollMany(18, 1);
            Assert.That(game.Score(), Is.EqualTo(29));
        }
        [Test()]
        public void RollSpareEveryFrame()
        {
            RollMany(21, 5);
            Assert.That(game.Score(), Is.EqualTo(150));
        }

        [Test()]
        public void RollStrikeEveryFrame()
        {
            RollMany(12,10);
            Assert.That(game.Score(), Is.EqualTo(300));
        }
        [Test()]
        public void RollSpareLastFrame()
        {
            RollMany(18, 1);
            game.Roll(9); game.Roll(1);
            game.Roll(9);
            Assert.That(game.Score(), Is.EqualTo(37));
        }
    }
}
